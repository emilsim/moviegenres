package main

import (
    "html/template"
    "net/http"
    "github.com/gorilla/mux"
    "os"
)

type Movie struct {
    Title  string
}

var tmpls = template.Must(template.ParseFiles("src/movie/templates/index.html"))

func main() {
    port := os.Getenv("PORT")
    if port == ""{
	    port = "8080"
    }


    router := mux.NewRouter()
    //router.PathPrefix("/home/emil/go/src/diary/templates/styles").Handler(http.StripPrefix("/home/emil/go/src/diary/templates/styles",http.FileServer(http.Dir("/home/emil/go/src/diary/templates/styles"))))
    router.HandleFunc("/Horror", HorrorMovie)
    router.HandleFunc("/Action", ActionMovie)
    router.HandleFunc("/Comedy", ComedyMovie)
    router.HandleFunc("/Drama", DramaMovie)
    router.HandleFunc("/", ShowInformation)
    http.Handle("/", router)

    http.ListenAndServe(":"+port, nil)
}

func HorrorMovie(w http.ResponseWriter, r *http.Request) {
       var tmpl = template.Must(template.ParseFiles("src/movie/templates/horror.html"))


	movie := Movie{"Horror"}
    if err:= tmpl.ExecuteTemplate(w, "horror.html", movie); err != nil{
       http.Error(w, err.Error(), http.StatusInternalServerError)
    }

}

func ShowInformation(w http.ResponseWriter, r *http.Request) {
	//movies := []string{"Horror", "Comedy", "Action", "Drama", "Fantasy"}
    //if err := tmpl.Execute(w, ""); err != nil {
    if err:= tmpls.ExecuteTemplate(w, "index.html", ""); err != nil{
       http.Error(w, err.Error(), http.StatusInternalServerError)
    }
}

func ActionMovie(w http.ResponseWriter, r *http.Request) {
       var tmpl = template.Must(template.ParseFiles("src/movie/templates/action.html"))
	movie := Movie{"Action"}
    if err:= tmpl.ExecuteTemplate(w, "action.html", movie); err != nil{
       http.Error(w, err.Error(), http.StatusInternalServerError)
    }

}
func ComedyMovie(w http.ResponseWriter, r *http.Request) {
       var tmpl = template.Must(template.ParseFiles("src/movie/templates/comedy.html"))
	movie := Movie{"Comedy"}
    if err:= tmpl.ExecuteTemplate(w, "comedy.html", movie); err != nil{
       http.Error(w, err.Error(), http.StatusInternalServerError)
    }

}

func DramaMovie(w http.ResponseWriter, r *http.Request) {
       var tmpl = template.Must(template.ParseFiles("src/movie/templates/drama.html"))
	movie := Movie{"Drama"}
    if err:= tmpl.ExecuteTemplate(w, "drama.html", movie); err != nil{
       http.Error(w, err.Error(), http.StatusInternalServerError)
    }

}
